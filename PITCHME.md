---
marp: true
---


## Aconet TBG 61
#### Asset Management

---
## Auf der Suche nach der Wahrheit

---
## Agenda


---
## neues Gebäude
* Wie viele Ports werden benötigt ?
* Was ist nun unser neues "Standard" Modell ?
* Reichen die Features die der Hersteller bietet ?
  
---
## Beschaffung

* Modell (Modellnummer)
* Inventarisierung (Seriennummer)
* Wartungvertrag (Kontakdaten/Laufzeit)
* Liezenz (Key oder Zertifikat)

---
## Assets
* wenig Daten
* statisch
* oft Physik

---
#### Muss man vielleicht nicht alles selber Pflegen

![Netbox Überblick](_images/netbox_device_libary.png)

https://github.com/netbox-community/devicetype-library

---
## Installation
* Gebäude
* Stockwerk
* Stromkreis

---
## Assets
* wenig Daten
* statisch
* oft Physik

---
## Netbox

![Netbox Überblick](_images/netbox_überblick.png)

---
## Inbetriebnahme
* Initial Config
* Portconfig
  
---
## Assets
* schon etwas mehr Daten
* nicht mehr so statisch
* noch Physik
  
---
## Betrieb
##### Es lebt und die Welt ist Bunt
* Config Änderungen
* Switchport X
* VLAN Y
* IP Adresse

---
## Assets
* viele Daten
* dynamisch
* eher Virtuell

---
## Netbox REST API

![Netbox Überblick](_images/netbox_rest_api.png)

---
## Warum Asset Management
#### Fragen
* Kollege
  * Wie war die Seriennummer des Switches im Stockwerk XY ?
* Zertifizierung
  * Wer hat die Änderung XY gemacht ?
* Gesetzliche Vorgaben (DSGVO)
  * Welche Persönlichen Daten sind auf Server X gespeichert ?

---
## Tools

* Kopf
* Notepad
* Excel
* vielleicht doch ein Stück Software

---
## Wer sagt die Warheit

| Data Domain| Warheit (Beispiel) | 	Owner (Beispiel) |
|:---|:---:|:---:|
|Beschaffung |	ERP |	IT Einkauf |
|Serviceverträge |	ERP |	IT |
|Imobilien | --- | Immo |
| Kundendaten | Helpline | IT Servicedesk|
| Device Inventory | PHPIPAM (netbox)  | IT NuS |
| IP Adressen | PHPIPAM (netbox) | IT NuS |
| Konfiguration | Gitlab (netbox) | IT Nus |

---
## Konzept
![Netbox Überblick](_images/konzept.png)
  
---
## Gedanken
#### Was mache ich dann mit 

* Reporting
* Monitoring Daten

---
## Und bitte nicht vergessen
#### Die einzige Konstante ist die Veränderung

* Es ist also immer nur eine Momentaufnahme
* Jeder darf/soll/muss Asset Management in seinen Arbeitsablauf einbauen 

---
# ???

---
## Links
* Netbox Device Libary https://github.com/netbox-community/devicetype-library
  
* Netbox as a Source of truth: https://www.youtube.com/watch?v=GyQf5F0gr3w&list=WL&index=102&t=5s
  
---
## Notizen
